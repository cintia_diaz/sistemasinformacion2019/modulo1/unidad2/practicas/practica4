﻿/* PRACTICA NUMERO: 4 */

USE practica4;



/* CONSULTA 1 */
  -- Averigua el DNI de todos los clientes
  SELECT 
    c.dni 
  FROM cliente c;

/* CONSULTA 2 */
  -- Consulta todos los datos de todos los programas
  SELECT
    * 
  FROM programa p;

/* CONSULTA 3 */
  -- Obtén un listado con los nombres de todos los programas.
  SELECT 
    DISTINCT p.nombre 
  FROM programa p;

/* CONSULTA 4 */
  -- Genera una lista con todos los comercios
  SELECT 
    * 
  FROM comercio c;

/* CONSULTA 5 */
  -- Genera una lista de las ciudades con establecimientos donde se venden programas, sin que aparezcan valores duplicados
  -- (utiliza DISTINCT)
  SELECT 
    DISTINCT c.ciudad 
  FROM programa p 
  JOIN distribuye d 
  USING (codigo) 
  JOIN comercio c 
  USING (cif);

/* CONSULTA 6 */
  -- Obtén una lista con los nombres de programas, sin que aparezcan valores duplicados (utiliza DISTINCT)
  SELECT 
    DISTINCT p.nombre 
  FROM programa p;


/* CONSULTA 7 */
  -- Obtén el DNI más 4 de todos los clientes.
  SELECT 
    c.dni+4 
  FROM cliente c;


/* CONSULTA 8 */ 
  -- Haz un listado con los códigos de los programas multiplicados por 7.
  SELECT 
    p.codigo*7 
  FROM programa p;
   
/* CONSULTA 9 */
  -- ¿Cuáles son los programas cuyo código es inferior o igual a 10?
  SELECT 
    p.nombre, p.version 
  FROM programa p 
  WHERE p.codigo <= 10;


/* CONSULTA 10 */
  -- ¿Cuál es el programa cuyo código es 11?
  SELECT 
    p.nombre, p.version
  FROM programa p 
  WHERE p.codigo=11;


/* CONSULTA 11 */
  -- ¿Qué fabricantes son de Estados Unidos?
  SELECT
    * 
  FROM fabricante f 
  WHERE f.pais='Estados Unidos';

/* CONSULTA 12 */
  --  ¿Cuáles son los fabricantes no españoles? Utilizar el operador IN.
  SELECT 
    * 
  FROM fabricante f 
  WHERE f.pais NOT IN ('ESPAÑA');


/* CONSULTA 13 */
  -- Obtén un listado con los códigos de las distintas versiones de Windows
  SELECT 
    p.codigo 
  FROM programa p
  WHERE p.nombre ='Windows';


/* CONSULTA 14 */
  -- ¿En qué ciudades comercializa programas El Corte Inglés?
  SELECT
    c.ciudad 
  FROM comercio c 
  WHERE c.nombre ='El corte inglés';


/* CONSULTA 15 */
  -- ¿Qué otros comercios hay, además de El Corte Inglés? Utilizar el operador IN
  SELECT 
    * 
  FROM comercio c 
  WHERE c.nombre  
  NOT IN ('El corte Inglés');


/* CONSULTA 16 */
  -- Genera una lista con los códigos de las distintas versiones de Windows y Access. Utilizar el operador IN
  SELECT
    p.codigo
  FROM programa p 
  WHERE p.nombre IN ('Windows', 'Access');

/* CONSULTA 17 */
  -- Obtén un listado que incluya los nombres de los clientes de edades comprendidas entre 10 y 25 y de los mayores de 50 años.
  -- Da una solución con BETWEEN y otra sin BETWEEN
-- con BETWEEN
  SELECT 
    c.nombre, c.edad 
  FROM cliente c 
  WHERE c.edad 
  BETWEEN 10 AND 25
  OR c.edad >=50;
 

-- sin BETWEEN
  SELECT 
    c.nombre 
  FROM cliente c 
  WHERE c.edad >=10 AND c.edad<=25 
  OR c.edad>=50;

/* CONSULTA 18 */
  -- Saca un listado con los comercios de Sevilla y Madrid. No se admiten valores duplicados.

  SELECT DISTINCT nombre FROM comercio c WHERE c.ciudad='Sevilla' 
  UNION 
  SELECT DISTINCT c.nombre FROM comercio c WHERE c.ciudad='Madrid';

/* CONSULTA 19 */
  -- ¿Qué clientes terminan su nombre en la letra “o”?
  SELECT 
    c.nombre
  FROM cliente c 
  WHERE c.nombre LIKE '%o';

/* CONSULTA 20 */
  -- ¿Qué clientes terminan su nombre en la letra “o” y, además, son mayores de 30 años?
  SELECT 
    * 
  FROM cliente c
  WHERE c.nombre LIKE '%o' 
  AND c.edad>=30;


/* CONSULTA 21 */
  -- Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A
  -- o por una W.
  SELECT 
    * 
  FROM programa p 
  WHERE p.VERSION LIKE '%i' 
  OR p.nombre LIKE 'A%' 
  OR p.nombre LIKE 'W%';

/* CONSULTA 22 */
  -- Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A
  -- y termine por una S
  SELECT * 
  FROM programa p 
  WHERE p.version LIKE '%i' 
  OR p.nombre LIKE 'A%S';


/* CONSULTA 23 */
  -- Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, y cuyo nombre no comience por una A
  SELECT 
    * 
  FROM programa p 
  WHERE p.VERSION LIKE '%i' 
  AND p.nombre NOT LIKE 'A%';


/* CONSULTA 24 */
  -- Obtén una lista de empresas por orden alfabético ascendente.
  SELECT
    * 
  FROM fabricante f 
  ORDER BY f.nombre ASC;

/* CONSULTA 25 */
  -- Genera un listado de empresas por orden alfabético descendente.
  SELECT 
    * 
  FROM fabricante f 
  ORDER BY f.nombre DESC;

/* CONSULTA 26 */
  -- Obtén un listado de programas por orden de versión
  SELECT 
    * 
  FROM programa p 
  ORDER BY p.version;

  SELECT * FROM programa p ORDER BY p.nombre, p.version;


/* CONSULTA 27 */
  -- Genera un listado de los programas que desarrolla Oracle

  -- c1: id_fab del fabricante Oracle
  SELECT 
    f.id_fab 
  FROM fabricante f 
  WHERE f.nombre ='Oracle';

  -- c2: codigo de los programas que desarrolla Oracle
  SELECT 
    d.codigo 
  FROM desarrolla d 
  WHERE d.id_fab=(SELECT f.id_fab FROM fabricante f WHERE f.nombre ='Oracle');

  -- final: nombre y version de los programas
  SELECT 
    p.nombre, p.version
  FROM programa p 
  JOIN (SELECT d.codigo FROM desarrolla d WHERE d.id_fab=(SELECT f.id_fab FROM fabricante f WHERE f.nombre ='Oracle')) c1 
  USING (codigo);


/* CONSULTA 28 */   
  -- ¿Qué comercios distribuyen Windows? 
  
  -- c1: codigo de Windows
  SELECT
    p.codigo
  FROM programa p 
  WHERE p.nombre = 'Windows';

  -- final: 
  SELECT 
    d.cif, c.nombre, c.ciudad
  FROM distribuye d 
  JOIN (SELECT p.codigo FROM programa p WHERE p.nombre = 'Windows') c1 
  USING (codigo)
  JOIN comercio c 
  USING (cif);


/* CONSULTA 29 */
  -- Genera un listado de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid.
  -- c1: cif de 'El Corte Inglés' de Madrid
  SELECT
    c.cif 
  FROM comercio c 
  WHERE c.nombre ='El Corte Inglés' AND c.ciudad='Madrid';


-- final: 
  SELECT
    d.codigo,p.nombre,p.version,d.cantidad 
  FROM distribuye d 
  JOIN (SELECT c.cif FROM comercio c WHERE c.nombre ='El Corte Inglés' AND c.ciudad='Madrid') c1
  USING (cif)
  JOIN programa p 
  USING (codigo);

/* CONSULTA 30 */
  -- ¿Qué fabricante ha desarrollado Freddy Hardest?
  
  -- c1: codigo del programa
  SELECT p.codigo 
  FROM programa p 
  WHERE p.nombre='Freddy Hardest';

  -- final
  SELECT
    f.nombre
  FROM (SELECT p.codigo FROM programa p WHERE p.nombre='Freddy Hardest') c1 
  JOIN desarrolla d 
  USING (codigo)
  JOIN fabricante f 
  USING (id_fab);



/* CONSULTA 31 */
-- Selecciona el nombre de los programas que se registran por Internet.

  -- c1: codigo de los programas registrados por Internet  
  SELECT 
    DISTINCT r.codigo 
  FROM registra r 
  WHERE r.medio='Internet';

  -- final
  SELECT
    p.nombre
    FROM (
    SELECT DISTINCT r.codigo FROM registra r WHERE r.medio='Internet'
      ) c1 
    JOIN programa p 
    USING (codigo);


/* CONSULTA 32 */
-- Selecciona el nombre de las personas que se registran por Internet.

  -- c1: dni de las personas que se registran por Internet
  SELECT 
    DISTINCT r.dni 
  FROM registra r 
  WHERE r.medio='Internet';

  -- final
  SELECT
    c.nombre 
    FROM (
    SELECT DISTINCT r.dni FROM registra r WHERE r.medio='Internet'
    ) c1
    JOIN cliente c
  USING (dni);

/* CONSULTA 33 */
-- ¿Qué medios ha utilizado para registrarse Pepe Pérez?

  -- c1: dni de Pepe Pérez
  SELECT
    c.dni 
  FROM cliente c 
  WHERE c.nombre='Pepe Pérez';

  -- final
  SELECT 
    DISTINCT r.medio 
  FROM 
    (
    SELECT c.dni FROM cliente c WHERE c.nombre='Pepe Pérez'
    ) c1
  JOIN registra r
  USING (dni);


/* CONSULTA 34 */
-- ¿Qué usuarios han optado por Internet como medio de registro?

  -- c1: dni de los compradores por Internet
  SELECT 
    DISTINCT r.dni 
  FROM registra r 
  WHERE r.medio='Internet';

  -- final
  SELECT
    c.nombre 
  FROM (SELECT DISTINCT r.dni FROM registra r WHERE r.medio='Internet') c1 
  JOIN cliente c 
  USING (dni);


/* CONSULTA 35 */
-- ¿Qué programas han recibido registros por tarjeta postal?

  -- c1: codigo de los programas registrados por tarjeta postal
  
  SELECT
    DISTINCT r.codigo 
  FROM registra r 
  WHERE r.medio ='Tarjeta postal';

  -- final
  SELECT 
    p.nombre 
  FROM (SELECT DISTINCT r.codigo FROM registra r WHERE r.medio ='Tarjeta postal') c1 
  JOIN programa p 
  USING (codigo);


/* CONSULTA 36 */
-- ¿En qué localidades se han vendido productos que se han registrado por Internet?
  
  -- c1: comercios donde se han registrado productos por Internet
  SELECT DISTINCT r.cif FROM registra r WHERE r.medio='Internet';

  -- final
  SELECT 
    DISTINCT c.ciudad 
  FROM (SELECT DISTINCT r.cif FROM registra r WHERE r.medio='Internet') c1 
  JOIN comercio c 
  USING(cif);
  

/* CONSULTA 37 */
-- Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para
-- los que ha efectuado el registro.

  -- c1: codigos de clientes y programas que han usado internet para registrarse
  SELECT r.dni, r.codigo FROM registra r WHERE r.medio='Internet';

  -- final
  SELECT 
      c.nombre cliente,p.nombre programa, p.version 
   FROM (SELECT r.dni, r.codigo FROM registra r WHERE r.medio='Internet') c1 
   JOIN cliente c
   USING (dni)
   JOIN programa p 
   USING(codigo);


/* CONSULTA 38 */
-- Genera un listado en el que aparezca cada cliente junto al programa que ha registrado, el medio con el que lo ha hecho y el
-- comercio en el que lo ha adquirido.

  -- c1: de cliente me quedo con el dni y el nombre
  SELECT c.dni, c.nombre FROM cliente c;
 
  -- final: 
  SELECT 
    c1.nombre cliente, p.nombre programa, p.version,r.medio, c.nombre comercio, c.ciudad 
  FROM (SELECT c.dni, c.nombre FROM cliente c) c1 
  JOIN registra r 
  USING (dni) 
  JOIN programa p 
  USING(codigo) 
  JOIN comercio c 
  USING(cif);  



/* CONSULTA 39 */
-- Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle.

  -- c1: id del fabricante de Oracle
  SELECT f.id_fab FROM fabricante f WHERE f.nombre='Oracle';

  -- final:
  SELECT DISTINCT ciudad 
    FROM (SELECT f.id_fab FROM fabricante f WHERE f.nombre='Oracle') c1 
    JOIN desarrolla 
      USING (id_fab)
    JOIN distribuye  
      USING (codigo) 
    JOIN comercio  
      USING (cif);


/* CONSULTA 40 */
-- Obtén el nombre de los usuarios que han registrado Access XP.

  -- c1: codigo del programa Access XP
  SELECT p.codigo FROM programa p WHERE p.nombre='access' AND p.VERSION='xp';

  -- final
  SELECT 
    c.nombre 
  FROM 
    (SELECT p.codigo FROM programa p WHERE p.nombre='access' AND p.VERSION='xp') c1
  JOIN registra r 
  USING (codigo) 
  JOIN cliente c
  USING (dni);

/* CONSULTA 41 */
-- Nombre de aquellos fabricantes cuyo país es el mismo que ʻOracleʼ. (Subconsulta).

  -- c1: id del fabricante y pais de Oracle
  SELECT f.id_fab, f.pais FROM fabricante f WHERE f.nombre='Oracle';

  -- final: 
  SELECT 
    f.nombre 
  FROM (SELECT f.id_fab, f.pais FROM fabricante f WHERE f.nombre='Oracle') c1
  JOIN fabricante f 
  ON c1.pais=f.pais AND c1.id_fab!=f.id_fab; 
 

/* CONSULTA 42 */
-- Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez. (Subconsulta).

  -- c1: dni y edad de Pepe Pérez
  SELECT c.dni, c.edad FROM cliente c WHERE c.nombre='Pepe Pérez';

  -- final: nombre de los clientes con la misma edad que Pepe Pérez pero con distinto dni
  SELECT 
    c.nombre 
  FROM (SELECT c.dni, c.edad FROM cliente c WHERE c.nombre='Pepe Pérez') c1 
  JOIN cliente c
  ON c1.edad=c.edad AND c.dni!=c1.dni;

/* CONSULTA 43 */
-- Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio ʻFNACʼ. (Subconsulta).

  -- c1: obtengo cif y ciudad de FNAC
  SELECT
    c.cif, c.ciudad 
  FROM comercio c
  WHERE c.nombre='FNAC';


  -- final: nombre de los comercios en la misma ciudad que FNAC
  SELECT 
    c.nombre 
  FROM (SELECT c.cif, c.ciudad FROM comercio c WHERE c.nombre='FNAC') c1 
  JOIN comercio c
  ON c1.ciudad=c.ciudad AND c1.cif!=c.cif;


/* CONSULTA 44 */
-- Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente ʻPepe Pérezʼ. (Subconsulta).

  -- c1: dni de Pepe Pérez
  SELECT c.dni FROM cliente c WHERE c.nombre='Pepe Pérez';
  -- c2: medios que ha registrado Pepe Pérez
  SELECT
    DISTINCT r.medio 
  FROM registra r
  WHERE dni=(SELECT c.dni FROM cliente c WHERE c.nombre='Pepe Pérez');

  -- final: nombres de las personas distintas de Pepe Pérez que registran un producto igual que Pepe Pérez
  SELECT 
    DISTINCT c.nombre    
  FROM registra r 
  JOIN (SELECT DISTINCT r.medio FROM registra r WHERE dni=(SELECT c.dni FROM cliente c WHERE c.nombre='Pepe Pérez')) c1 
  ON c1.medio=r.medio 
  JOIN cliente c 
  USING (dni) 
  WHERE dni!=(SELECT c.dni FROM cliente c WHERE c.nombre='Pepe Pérez');

/* CONSULTA 45 */
-- Obtener el número de programas que hay en la tabla programas.

  SELECT 
    COUNT(DISTINCT p.nombre)
  FROM programa p;

/* CONSULTA 46 */
-- Calcula el número de clientes cuya edad es mayor de 40 años.

  SELECT 
    COUNT(*) 
  FROM cliente c 
  WHERE c.edad>40;


/* CONSULTA 47 */
-- Calcula el número de productos que ha vendido el establecimiento cuyo CIF es 1.

  SELECT 
    COUNT(*) 
  FROM registra r 
  WHERE r.cif=1;
  

/* CONSULTA 48 */
-- Calcula la media de programas que se venden cuyo código es 7.
  SELECT 
    AVG(d.cantidad) 
  FROM distribuye d
  WHERE d.codigo=7;
  

/* CONSULTA 49 */
-- Calcula la mínima cantidad de programas de código 7 que se ha vendido
    
  SELECT 
    MIN(d.cantidad) 
  FROM distribuye d 
  WHERE d.codigo=7;
  

/* CONSULTA 50 */
-- Calcula la máxima cantidad de programas de código 7 que se ha vendido.

  SELECT 
    MAX(d.cantidad) 
  FROM distribuye d 
  WHERE d.codigo=7;

/* CONSULTA 51 */
-- ¿En cuántos establecimientos se vende el programa cuyo código es 7?

  SELECT 
    COUNT(*) 
  FROM distribuye d 
  WHERE d.codigo=7;


/* CONSULTA 52 */
--  Calcular el número de registros que se han realizado por Internet.
  SELECT 
    COUNT(*) 
  FROM registra r 
  WHERE r.medio='Internet';

/* CONSULTA 53 */
-- Obtener el número total de programas que se han vendido en ʻSevillaʼ

  -- c1: cif de los comercios de Sevilla
  SELECT
    c.cif 
  FROM comercio c
  WHERE c.ciudad='Sevilla';

  -- consulta final
  SELECT 
    COUNT(d.codigo) 
  FROM (
    SELECT c.cif FROM comercio c WHERE c.ciudad='Sevilla'
  ) c1 
  JOIN distribuye d 
  USING (cif);
 
  
/* CONSULTA 54 */
-- Calcular el número total de programas que han desarrollado los fabricantes cuyo país es ʻEstados Unidosʼ.

-- c1: id_fab de los fabricantes de estados unidos
SELECT 
  f.id_fab 
FROM fabricante f 
WHERE f.pais='Estados Unidos';

-- consulta final:
SELECT COUNT(*) 
FROM (
  SELECT f.id_fab FROM fabricante f WHERE f.pais='Estados Unidos'
  ) c1 
JOIN desarrolla d 
USING (id_fab);


/* CONSULTA 55 */
-- Visualiza el nombre de todos los clientes en mayúscula. En el resultado de la consulta debe aparecer también la longitud de
-- la cadena nombre

SELECT 
  UPPER(c.nombre) nombre, 
  CHAR_LENGTH(c.nombre) longitud 
FROM cliente c;


/* CONSULTA 56 */
-- Con una consulta concatena los campos nombre y versión de la tabla PROGRAMA

  SELECT 
    CONCAT(p.nombre,' ', p.version) 
  FROM programa p;


